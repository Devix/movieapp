package com.android.movieapp.domain.usecase

import android.location.Location
import com.android.movieapp.domain.model.Album
import com.android.movieapp.domain.model.Movie
import com.android.movieapp.domain.repository.AlbumRepository
import com.android.movieapp.domain.usecase.base.SingleUseCase
import io.reactivex.Single
import javax.inject.Inject


/**
 * Created by Carlos Anguiano on 25/02/2022.
 * c.joseanguiano@gmail.com
 */
class GetAlbumsMovieUseCase @Inject constructor(private val repository: AlbumRepository) :
    SingleUseCase<Album>() {
    override fun buildUseCaseSingle(): Single<Album> {
        return repository.getAlbumsMovie()
    }

    fun saveMovie(it: Album) {
        repository.saveAllMovies(it.results)
    }

    fun getAlbumMovie(): List<Movie> {
        return repository.getAllAlbumLocal()
    }

    fun uploadLocation(location: Location) {
        return repository.uploadLocationFirebase(location)
    }


}