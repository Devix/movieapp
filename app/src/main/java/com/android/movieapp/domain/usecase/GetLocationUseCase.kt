package com.android.movieapp.domain.usecase

import com.android.movieapp.domain.model.LocationLocal
import com.android.movieapp.domain.repository.LocationRepository
import javax.inject.Inject


/**
 * Created by Carlos Anguiano on 27/02/2022.
 * c.joseanguiano@gmail.com
 */
class GetLocationUseCase @Inject constructor(private val repository: LocationRepository) {


    suspend fun loadLocation(success: (listLocation: MutableList<LocationLocal>?) -> Unit) {
        repository.getPotatoesData {
            success(it)
        }
    }
}
