package com.android.movieapp.domain.model


/**
 * Created by Carlos Anguiano on 27/02/2022.
 * c.joseanguiano@gmail.com
 */
data class LocationLocal(
    var latitude: String = "",
    var longitude: String = "",
    var dateLocation: String = ""
) {

    override fun toString(): String {
        return "$latitude $longitude $dateLocation"
    }
}