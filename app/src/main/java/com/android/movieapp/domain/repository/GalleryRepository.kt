package com.android.movieapp.domain.repository

import android.net.Uri


/**
 * Created by Carlos Anguiano on 27/02/2022.
 * c.joseanguiano@gmail.com
 */
interface GalleryRepository {
    suspend fun uploadImageToFirebase(currentPhotoPath: Uri,successs: (potatoesData: Boolean?) -> Unit)
}