package com.android.movieapp.domain.usecase

import android.net.Uri
import com.android.movieapp.domain.repository.GalleryRepository
import javax.inject.Inject


/**
 * Created by Carlos Anguiano on 27/02/2022.
 * c.joseanguiano@gmail.com
 */
class GetImageUseCase @Inject constructor(private val repository: GalleryRepository) {

    suspend fun uploadImage(currentPhotoPath: Uri,successs: (potatoesData: Boolean?) -> Unit) {
        repository.uploadImageToFirebase(currentPhotoPath){
            successs(it)
        }
    }
}
