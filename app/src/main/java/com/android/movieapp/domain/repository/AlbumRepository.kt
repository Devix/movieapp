package com.android.movieapp.domain.repository

import android.location.Location
import com.android.movieapp.domain.model.Album
import com.android.movieapp.domain.model.Movie
import io.reactivex.Single


/**
 * Created by Carlos Anguiano on 25/02/2022.
 * c.joseanguiano@gmail.com
 */
interface AlbumRepository {

    fun getAlbumsMovie(): Single<Album>

    fun saveAllMovies(results: List<Movie>)

    fun getAllAlbumLocal(): List<Movie>

    fun uploadLocationFirebase(location: Location)

}