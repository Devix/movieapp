package com.android.movieapp.domain.repository

import com.android.movieapp.domain.model.LocationLocal


/**
 * Created by Carlos Anguiano on 25/02/2022.
 * c.joseanguiano@gmail.com
 */
interface LocationRepository {
    suspend fun getPotatoesData(success: (listLocation: ArrayList<LocationLocal>?) -> Unit)

}