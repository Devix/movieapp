package com.android.movieapp.domain.model

import com.google.gson.annotations.SerializedName


/**
 * Created by Carlos Anguiano on 25/02/2022.
 * c.joseanguiano@gmail.com
 */

data class Movie(
    @SerializedName("id")
    var id: Int,
    @SerializedName("poster_path")
    var posterPath: String,
    @SerializedName("title")
    var title: String
)