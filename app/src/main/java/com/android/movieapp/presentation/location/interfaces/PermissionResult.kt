package com.android.movieapp.presentation.location.interfaces

import android.content.pm.PackageManager

data class PermissionResult(val result: Map<String, Int>, val requestCode: Int) {

    fun areAllGranted(): Boolean {
        var allGranted = true
        result.values.forEach {
            if (it != PackageManager.PERMISSION_GRANTED) {
                allGranted = false
            }
        }
        return allGranted
    }

    fun isOneGranted(): Boolean {
        var oneGranted = false
        result.values.forEach {
            if (it == PackageManager.PERMISSION_GRANTED) {
                oneGranted = true
            }
        }
        return oneGranted
    }

    fun getGranted(): List<String> {
        return result.filter { it.value == PackageManager.PERMISSION_GRANTED }.map { it.key }
    }

    fun getDenied(): List<String> {
        return result.filter { it.value == PackageManager.PERMISSION_DENIED }.map { it.key }
    }

}