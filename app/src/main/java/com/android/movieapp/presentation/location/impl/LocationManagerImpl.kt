package com.android.movieapp.presentation.location.impl

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.provider.Settings
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.movieapp.presentation.location.interfaces.LocationManager
import com.android.movieapp.presentation.location.interfaces.PermissionManager
import com.android.movieapp.presentation.location.models.LocationTrackerConfig
import com.android.movieapp.presentation.location.models.PermissionRequestExplanation
import com.google.android.gms.location.*

class LocationManagerImpl : LocationManager {

    private var finishActivity: Boolean = false
    private var fusedLocationProviderClient: FusedLocationProviderClient? = null
    private var locationRequest: LocationRequest? = null
    private var lastTrackedLocation: Location? = null
    private var locationCallback: LocationCallback? = null
    private val permissionManager: PermissionManager = PermissionManagerImpl
    private val locationLiveData = MutableLiveData<Location?>()
    private var activity: Activity? = null

    private fun initLocationManager(activity: Activity) {

        this.activity = activity

        if (fusedLocationProviderClient == null) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity)
        }
    }

    override fun gspActivado(activity: Activity): Boolean {
        try {
            val lm =
                activity.getSystemService(Context.LOCATION_SERVICE) as android.location.LocationManager

            val gps_enabled: Boolean = lm.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER)
            val network_enabled: Boolean = lm.isProviderEnabled(android.location.LocationManager.NETWORK_PROVIDER)


            return !(!gps_enabled && !network_enabled)
        } catch (ex: Exception) {
            Log.d(javaClass.simpleName, ex.message, ex)
        }
        return false
    }

    private fun checkIfLocationProviderIsEnabled(requestCode: Int): Boolean {
        try {
            val lm =
                activity?.getSystemService(Context.LOCATION_SERVICE) as android.location.LocationManager

            val gps_enabled: Boolean = lm.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER)
            val network_enabled: Boolean = lm.isProviderEnabled(android.location.LocationManager.NETWORK_PROVIDER)

            if (!gps_enabled && !network_enabled) {
                activity?.let {
                    run {
                        val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS).apply {
                            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                            addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
                        }
                        activity?.startActivityForResult(intent, requestCode)
                    }
                    if (finishActivity) {
                        activity?.finish()
                    }
                }
            } else {
                return true
            }
        } catch (ex: Exception) {
            Log.d(javaClass.simpleName, ex.message, ex)
        }
        return false
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ): Boolean? {
        if (requestCode == 17 && grantResults[0] == -1) {
            if (finishActivity) {
                activity?.finish()
            }
        }
        return permissionManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun getLastKnownLocation(
        activity: Activity,
        checkProviderEnabled: Boolean
    ): LiveData<Location?> {

        initLocationManager(activity)

        permissionManager.checkPermissions(
            activity = activity,
            permissions = arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ),
            onPermissionResult = { permissionResult ->
                if (permissionResult.areAllGranted()) {
                    if (checkProviderEnabled && checkIfLocationProviderIsEnabled(
                            REQUEST_CHECK_SETTINGS_FOR_LAST_LOCATION
                        )
                    ) {
                        requestLastKnownLocation(activity)
                    } else {
                        requestLastKnownLocation(activity)
                    }
                }
            },
            requestCode = REQUEST_PERMISSION_LAST_KNOWN_LOCATION
        )

        return locationLiveData
    }

    override fun startLocationTracker(
        activity: Activity,
        config: LocationTrackerConfig,
        checkProviderEnabled: Boolean,
        closeActivity: Boolean
    ): LiveData<Location?> {

        initLocationManager(activity)
        this.finishActivity = closeActivity
        locationRequest = LocationRequest().apply {
            interval = config.interval
            fastestInterval = config.fastestInterval
            priority = config.priority
        }

        permissionManager.checkPermissions(
            activity = activity,
            permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            onPermissionResult = { permissionResult ->
                if (permissionResult.areAllGranted()) {
                    if (checkProviderEnabled && checkIfLocationProviderIsEnabled(
                            REQUEST_CHECK_SETTINGS_FOR_LOCATION_TRACKER
                        )
                    ) {
                        requestLocationUpdates()
                    } else {
                        requestLocationUpdates()
                    }
                }
            },
            permissionRequestPreExecuteExplanation = PermissionRequestExplanation(
                title = "",
                message = ""
            ),
            permissionRequestRetryExplanation = PermissionRequestExplanation(
                title = "",
                message = ""
            ),
            requestCode = REQUEST_PERMISSION_FINE_LOCATION_FOR_LOCATION_TRACKER
        )
        return locationLiveData
    }

    override fun stopLocationTracker() {
        locationCallback?.let {
            fusedLocationProviderClient?.removeLocationUpdates(it)
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestLastKnownLocation(activity: Activity?) {
        fusedLocationProviderClient
            ?.lastLocation
            ?.addOnSuccessListener { location: Location? ->
                locationLiveData.postValue(location)
            } ?: locationLiveData.postValue(null)
    }

    @SuppressLint("MissingPermission")
    private fun requestLocationUpdates() {
        fusedLocationProviderClient
            ?.lastLocation
            ?.addOnSuccessListener { location: Location? ->
                locationLiveData.postValue(location)
            }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {
                    if (location.latitude != lastTrackedLocation?.latitude
                        || location.longitude != lastTrackedLocation?.longitude
                        || location.accuracy != lastTrackedLocation?.accuracy
                    ) {
                        locationLiveData.postValue(location)
                        lastTrackedLocation = location
                    }
                }
            }
        }
        fusedLocationProviderClient?.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    @SuppressLint("ApplySharedPref")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean? {
        return if (requestCode == REQUEST_CHECK_SETTINGS_FOR_LAST_LOCATION && resultCode == PackageManager.PERMISSION_GRANTED) {
            requestLastKnownLocation(this.activity)
            true
        } else if (requestCode == REQUEST_CHECK_SETTINGS_FOR_LOCATION_TRACKER && resultCode == PackageManager.PERMISSION_GRANTED) {
            requestLocationUpdates()
            true
        } else {
            false
        }
    }


    override fun checkLocationPermission(): Boolean? {
        return activity?.let {
            permissionManager.checkPermissions(
                activity = it,
                permissions = arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                onPermissionResult = {},
                requestCode = REQUEST_PERMISSION_FINE_LOCATION_FOR_LOCATION_TRACKER
            )
        }
    }

    companion object {
        const val REQUEST_PERMISSION_LAST_KNOWN_LOCATION = 0x000010
        const val REQUEST_PERMISSION_FINE_LOCATION_FOR_LOCATION_TRACKER = 0x000011
        const val REQUEST_CHECK_SETTINGS_FOR_LOCATION_TRACKER = 0x000012
        const val REQUEST_CHECK_SETTINGS_FOR_LAST_LOCATION = 0x000013
    }
}