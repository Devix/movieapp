package com.android.movieapp.presentation.gallery

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.core.content.FileProvider
import androidx.fragment.app.viewModels
import com.android.movieapp.R
import com.android.movieapp.databinding.FragmentGalleryBinding
import com.android.movieapp.presentation.base.BaseFragment
import com.android.movieapp.util.Constants
import dagger.hilt.android.AndroidEntryPoint
import pub.devrel.easypermissions.EasyPermissions
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class GalleryFragment : BaseFragment(), EasyPermissions.PermissionCallbacks {

    private val galleryModel: GalleryViewModel by viewModels()
    private lateinit var viewDataBinding: FragmentGalleryBinding
    private lateinit var currentPhotoPath: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewDataBinding =
            FragmentGalleryBinding.inflate(inflater, container, false)
                .apply {
                    viewmodel = galleryModel
                    lifecycleOwner = this@GalleryFragment.viewLifecycleOwner
                }

        return viewDataBinding.root
    }


    override fun setupViewModelObservers() {
        galleryModel.goToCamera.observe(viewLifecycleOwner) {
            captureImage()
        }


        galleryModel.goToGallery.observe(viewLifecycleOwner) {
            selectImageFromGallery()
        }

        galleryModel.isSendImage.observe(viewLifecycleOwner) {
            if (it) {
                Toast.makeText(activity, getString(R.string.image_upload), Toast.LENGTH_SHORT)
                    .show()
            } else {
                Toast.makeText(activity, getString(R.string.image_failed), Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }

    private fun captureImage() {
        activity?.let { act ->
            if (hasCameraPermission(act)) {
                Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
                    takePictureIntent.resolveActivity(act.packageManager)?.also {
                        val photoFile: File? = try {
                            createImageFile()
                        } catch (ex: IOException) {
                            null
                        }
                        photoFile?.also {
                            val photoURI: Uri = FileProvider.getUriForFile(
                                act,
                                act.applicationContext.packageName.toString() + ".provider",
                                it
                            )
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                            startActivityForResult(takePictureIntent, Constants.CODE_RESULT_CAMERA)
                        }
                    }
                }
            } else {
                EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.permission_camera_message),
                    Constants.PERMISSION_CAMERA,
                    *PERMISSION_CAMERA
                )
            }
        }
    }

    private fun selectImageFromGallery() {
        activity?.let {
            if (hasReadStoragePermission(it)) {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(
                    Intent.createChooser(
                        intent,
                        "Please select..."
                    ),
                    Constants.CODE_RESULT_GALLERY
                )
            } else {
                EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.permission_camera_message),
                    Constants.PERMISSION_CAMERA,
                    *PERMISSION_CAMERA
                )
            }
        }


    }

    private fun hasCameraPermission(context: Context): Boolean {
        return EasyPermissions.hasPermissions(
            context,
            *PERMISSION_CAMERA
        )
    }

    private fun hasReadStoragePermission(context: Context): Boolean {
        return EasyPermissions.hasPermissions(
            context,
            *PERMISSION_STORAGE
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        activity?.let {
            if (requestCode == Constants.CODE_RESULT_GALLERY && resultCode == Activity.RESULT_OK) {
                val uri = data?.data
                if (uri != null) {
                    galleryModel.uploadImageCamera(uri)
                }
            } else if (requestCode == Constants.CODE_RESULT_CAMERA && resultCode == Activity.RESULT_OK) {
                try {
                    val uri = Uri.fromFile(File(currentPhotoPath))
                    galleryModel.uploadImageCamera(uri)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsGranted(requestCode: Int, @NonNull perms: List<String?>) {

    }

    override fun onPermissionsDenied(requestCode: Int, @NonNull perms: List<String?>) {
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp: String =
            SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())
        val storageDir: File? =
            activity?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)

        return File.createTempFile(
            "JPEG_${timeStamp}_",
            ".jpg",
            storageDir
        ).apply {
            currentPhotoPath = absolutePath
        }
    }

    companion object {
        private val PERMISSION_CAMERA =
            arrayOf(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )

        private val PERMISSION_STORAGE =
            arrayOf(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
    }


}
