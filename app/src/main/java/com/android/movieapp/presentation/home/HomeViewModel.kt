package com.android.movieapp.presentation.home

import android.location.Location
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.movieapp.domain.model.Movie
import com.android.movieapp.domain.usecase.GetAlbumsMovieUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


/**
 * Created by Carlos Anguiano on 25/02/2022.
 * c.joseanguiano@gmail.com
 */
@HiltViewModel
class HomeViewModel @Inject constructor(private val getAlbumsMovieUseCase: GetAlbumsMovieUseCase) :
    ViewModel() {

    val moviesPopularLiveData = MutableLiveData<List<Movie>>()
    val moviesTheaterLiveData = MutableLiveData<List<Movie>>()
    val moviesComedyLiveData = MutableLiveData<List<Movie>>()
    val isLoad = MutableLiveData<Boolean>()

    private val _goToShowAllEvent = MutableLiveData<Boolean>()
    val goToShowAllEvent: LiveData<Boolean> get() = _goToShowAllEvent

    init {
        isLoad.value = false

        getAlbumsMovieUseCase.execute(onSuccess = {
            isLoad.value = true
            moviesPopularLiveData.value = it.results
            moviesTheaterLiveData.value = it.results
            moviesComedyLiveData.value = it.results
            getAlbumsMovieUseCase.saveMovie(it)
        }, onError = {
            it.printStackTrace()
            moviesPopularLiveData.value = getAlbumsMovieUseCase.getAlbumMovie()
            moviesTheaterLiveData.value = getAlbumsMovieUseCase.getAlbumMovie()
            moviesComedyLiveData.value = getAlbumsMovieUseCase.getAlbumMovie()
        }, onFinished = {
            Log.i("loadAlbums", "onFinished ")
        })

    }

    fun goToShowAllPressed() {
        _goToShowAllEvent.value = true
    }

    fun uploadLocation(location: Location) {
        getAlbumsMovieUseCase.uploadLocation(location)
    }

}