package com.android.movieapp.presentation.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.location.Location
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.android.movieapp.R
import com.android.movieapp.util.Constants


/**
 * Created by Carlos Anguiano on 27/02/2022.
 * c.joseanguiano@gmail.com
 */


fun NotificationManager.sendNotification(context: Context, location: Location) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val channel = NotificationChannel(
            Constants.ID_NOTIFICATION,
            Constants.NOTIFICATION,
            NotificationManager.IMPORTANCE_DEFAULT
        )
        val manager = context.getSystemService(NotificationManager::class.java)
        manager?.createNotificationChannel(channel)
    }

    val text = context.getString(
        R.string.description_notification,
        location.latitude.toString(),
        location.longitude.toString()
    )

    val builder = NotificationCompat.Builder(context, Constants.ID_NOTIFICATION).setSilent(true)
    builder.setContentTitle(context.getString(R.string.save_location))
    builder.setContentText(text)
    builder.setSmallIcon(R.drawable.ic_launcher_background)
    builder.setAutoCancel(true)

    val managerCompat = NotificationManagerCompat.from(context)
    managerCompat.notify(Constants.NOTIFICATION_ID, builder.build())
}

fun NotificationManager.cancelNotifications() {
    cancelAll()
}