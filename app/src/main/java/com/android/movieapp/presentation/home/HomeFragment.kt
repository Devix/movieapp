package com.android.movieapp.presentation.home

import android.Manifest
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.movieapp.R
import com.android.movieapp.databinding.FragmentHomeBinding
import com.android.movieapp.domain.model.Movie
import com.android.movieapp.presentation.base.BaseFragment
import com.android.movieapp.presentation.location.impl.LocationManagerImpl
import com.android.movieapp.presentation.location.interfaces.LocationManager
import com.android.movieapp.presentation.location.models.LocationTrackerConfig
import com.android.movieapp.presentation.notification.sendNotification
import com.android.movieapp.util.Constants
import dagger.hilt.android.AndroidEntryPoint
import pub.devrel.easypermissions.EasyPermissions


@AndroidEntryPoint
class HomeFragment : BaseFragment(), EasyPermissions.PermissionCallbacks {

    private lateinit var homeBinding: FragmentHomeBinding
    private val homeViewModel: HomeViewModel by viewModels()
    private var adapter: MovieAdapter? = null
    private val locationManager: LocationManager = LocationManagerImpl()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeBinding = FragmentHomeBinding.inflate(inflater, container, false).apply {
            viewmodel = homeViewModel
            lifecycleOwner = this@HomeFragment.viewLifecycleOwner
        }
        return homeBinding.root
    }


    override fun setupViewModelObservers() {
        adapter = MovieAdapter()
        homeBinding.popularRecycler.adapter = adapter
        homeBinding.theaterRecyclerView.adapter = adapter
        homeBinding.comedyRecyclerView.adapter = adapter

        homeBinding.popularRecycler.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        homeBinding.theaterRecyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)
        homeBinding.comedyRecyclerView.layoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false)




        homeViewModel.moviesPopularLiveData.observe(viewLifecycleOwner) {
            it?.let {
                initRecyclerView(
                    it
                )
            }
        }

        homeViewModel.moviesTheaterLiveData.observe(viewLifecycleOwner) {
            it?.let {
                initRecyclerView(it)
            }
        }

        homeViewModel.moviesComedyLiveData.observe(viewLifecycleOwner) {
            it?.let {
                initRecyclerView(it)
            }
        }
        homeViewModel.goToShowAllEvent.observe(viewLifecycleOwner) {
            val action = HomeFragmentDirections.actionNavigationHomeToDetailFragment()
            findNavController().navigate(action)
        }

        activity?.let {
            if (hasLocationPermissions(it)) {
                preparedLocation()
            } else {
                EasyPermissions.requestPermissions(
                    this,
                    getString(R.string.permission_location_message),
                    Constants.PERMISSION_LOCATION,
                    *LOCATION_PERMISSION
                )
            }
        }
    }

    private fun preparedLocation() {
        activity?.let {
            locationManager.startLocationTracker(it, LocationTrackerConfig(), true, false)
                .observe(
                    this
                ) { location ->
                    if (location != null) {
                        homeViewModel.uploadLocation(location)
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            val manager = it.getSystemService(NotificationManager::class.java)
                            manager?.sendNotification(it, location)
                        }
                    } else if (locationManager.gspActivado(it)) {
                        locationManager.stopLocationTracker()
                    }
                }
        }
    }

    private fun initRecyclerView(it: List<Movie>) {
        adapter?.submitList(it)
    }

    private fun hasLocationPermissions(context: Context): Boolean {
        return EasyPermissions.hasPermissions(
            context,
            *LOCATION_PERMISSION
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsGranted(requestCode: Int, @NonNull perms: List<String?>) {
        preparedLocation()
    }

    override fun onPermissionsDenied(requestCode: Int, @NonNull perms: List<String?>) {
    }

    companion object {
        private val LOCATION_PERMISSION =
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
    }
}
