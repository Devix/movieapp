package com.android.movieapp.util.extension

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.android.movieapp.R
import com.android.movieapp.util.Constants
import com.squareup.picasso.Picasso


/**
 * Created by Carlos Anguiano on 26/02/2022.
 * c.joseanguiano@gmail.com
 */
@BindingAdapter("bind_poster_path")
fun ImageView.bindPosterImageWithPicasso(path: String?) {
    if (path.isNullOrBlank()) {
        this.setImageResource(R.drawable.ic_image_not_found)
        return
    }
    Picasso.get().load(Constants.BASE_POST_MOVIE + path)
        .fit()
        .error(R.drawable.ic_image_not_found).into(this)

}