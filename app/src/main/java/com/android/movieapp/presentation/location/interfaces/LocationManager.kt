package com.android.movieapp.presentation.location.interfaces
import android.app.Activity
import android.content.Intent
import android.location.Location
import androidx.lifecycle.LiveData
import com.android.movieapp.presentation.location.models.LocationTrackerConfig

interface LocationManager {
    fun getLastKnownLocation(activity: Activity, checkProviderEnabled: Boolean = true): LiveData<Location?>
    fun startLocationTracker(activity: Activity, config: LocationTrackerConfig = LocationTrackerConfig(), checkProviderEnabled: Boolean = true, closeActivity: Boolean): LiveData<Location?>
    fun stopLocationTracker()
    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray): Boolean?
    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?): Boolean?
    fun gspActivado(activity: Activity): Boolean
    fun checkLocationPermission(): Boolean?
}