package com.android.movieapp.presentation.location.interfaces

import android.app.Activity
import com.android.movieapp.presentation.location.models.PermissionRequestExplanation

interface PermissionManager {
    companion object {
        const val DEFAULT_PERMISSION_REQUEST_CODE = 0x000014
    }

    fun checkPermissions(activity: Activity, permissions: Array<out String>, onPermissionResult: ((PermissionResult) -> Unit)?, permissionRequestPreExecuteExplanation: PermissionRequestExplanation? = null, permissionRequestRetryExplanation: PermissionRequestExplanation? = null, requestCode: Int? = null): Boolean
    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray): Boolean?
}