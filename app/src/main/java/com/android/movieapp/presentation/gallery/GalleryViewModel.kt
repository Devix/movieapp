package com.android.movieapp.presentation.gallery

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.movieapp.domain.usecase.GetImageUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject


/**
 * Created by Carlos Anguiano on 27/02/2022.
 * c.joseanguiano@gmail.com
 */
@HiltViewModel
class GalleryViewModel @Inject constructor(private val getImageUseCase: GetImageUseCase) :
    ViewModel() {

    private val _goToCamera = MutableLiveData<Boolean>()
    val goToCamera: LiveData<Boolean> get() = _goToCamera

    private val _goToGallery = MutableLiveData<Boolean>()
    val goToGallery: LiveData<Boolean> get() = _goToGallery

    private val _isSendImage = MutableLiveData<Boolean>()
    val isSendImage: LiveData<Boolean> get() = _isSendImage

    var job: Job? = null
    val exceptionHandler = CoroutineExceptionHandler { _, throwable ->

    }

    fun openCamera() {
        _goToCamera.value = true
    }

    fun openGallery() {
        _goToGallery.value = true
    }

    fun uploadImageCamera(currentPhotoPath: Uri) {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            getImageUseCase.uploadImage(currentPhotoPath) {
                _isSendImage.value = it
            }
        }
    }

}