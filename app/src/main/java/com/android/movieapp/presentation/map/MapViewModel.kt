package com.android.movieapp.presentation.map

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.movieapp.domain.model.LocationLocal
import com.android.movieapp.domain.usecase.GetLocationUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import javax.inject.Inject


/**
 * Created by Carlos Anguiano on 27/02/2022.
 * c.joseanguiano@gmail.com
 */
@HiltViewModel
class MapViewModel @Inject constructor(private val getLocationUseCase: GetLocationUseCase) :
    ViewModel() {

    val locationLiveData = MutableLiveData<List<LocationLocal>>()

    var job: Job? = null
    private val exceptionHandler = CoroutineExceptionHandler { _, throwable ->

    }

    init {
        job = CoroutineScope(Dispatchers.IO + exceptionHandler).launch {
            getLocationUseCase.loadLocation {
                locationLiveData.value = it
            }
        }
    }

}