package com.android.movieapp.presentation.location.models

import com.google.android.gms.location.LocationRequest

data class LocationTrackerConfig(
        val interval: Long = 300000,
        val fastestInterval: Long = 300000,
        val priority: Int = LocationRequest.PRIORITY_HIGH_ACCURACY)