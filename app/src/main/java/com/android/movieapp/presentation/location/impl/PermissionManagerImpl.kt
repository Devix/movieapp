package com.android.movieapp.presentation.location.impl


import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.android.movieapp.presentation.location.interfaces.PermissionManager
import com.android.movieapp.presentation.location.interfaces.PermissionResult
import com.android.movieapp.presentation.location.models.PermissionRequestExplanation

object PermissionManagerImpl : PermissionManager {
    private var usedRequestCode = PermissionManager.DEFAULT_PERMISSION_REQUEST_CODE
    private var onPermissionResult: ((PermissionResult) -> Unit)? = null

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray): Boolean? {
        if (requestCode == usedRequestCode) {
            if (grantResults.isNotEmpty()) {
                val result = mutableMapOf<String, Int>()
                permissions.forEachIndexed { index, permission ->
                    result.put(permission, grantResults[index])
                }
                onPermissionResult?.invoke(PermissionResult(result, requestCode))

            } else {
                val result = mutableMapOf<String, Int>()
                permissions.forEachIndexed { index, permission ->
                    result.put(permission, grantResults[index])
                }
                onPermissionResult?.invoke(PermissionResult(result, requestCode))
            }
            return true
        }
        return false
    }

    override fun checkPermissions(activity: Activity, permissions: Array<out String>, onPermissionResult: ((PermissionResult) -> Unit)?, permissionRequestPreExecuteExplanation: PermissionRequestExplanation?, permissionRequestRetryExplanation: PermissionRequestExplanation?, requestCode: Int?): Boolean {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.onPermissionResult = onPermissionResult
            usedRequestCode = requestCode ?: PermissionManager.DEFAULT_PERMISSION_REQUEST_CODE

            val notGrantedPermissionList = mutableListOf<String>()
            permissions.forEach { permission ->
                if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                    notGrantedPermissionList.add(permission)
                }
            }

            if (notGrantedPermissionList.isNotEmpty()) {
                var showExplanation = false
                notGrantedPermissionList.forEach {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, it)) {
                        showExplanation = true
                    }
                }

                if (!showExplanation && permissionRequestPreExecuteExplanation != null) {
                    showPermissionRequestExplanationHint(activity, permissionRequestPreExecuteExplanation, notGrantedPermissionList.toTypedArray(), usedRequestCode)
                    return true
                }

                if (showExplanation && permissionRequestRetryExplanation != null) {
                    showPermissionRequestExplanationHint(activity, permissionRequestRetryExplanation, notGrantedPermissionList.toTypedArray(), usedRequestCode)
                } else {
                    ActivityCompat.requestPermissions(activity, permissions, usedRequestCode)
                }
                return false
            } else {
                val result = mutableMapOf<String, Int>()
                permissions.forEachIndexed { _, permission ->
                    result.put(permission, PackageManager.PERMISSION_GRANTED)
                }
                onPermissionResult?.invoke(PermissionResult(result, usedRequestCode))
                return true
            }
        } else {
            val result = mutableMapOf<String, Int>()
            permissions.forEach { permission ->
                result.put(permission, PackageManager.PERMISSION_GRANTED)
            }
            onPermissionResult?.invoke(PermissionResult(result, usedRequestCode))
            return true
        }

    }

    private fun showPermissionRequestExplanationHint(activity: Activity, permissionRequestExplanation: PermissionRequestExplanation, permissions: Array<String>, requestCode: Int, hintOnly: Boolean = true) {

    }
}