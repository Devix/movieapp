package com.android.movieapp.presentation.location.models

data class PermissionRequestExplanation(val title: String = "", val message: String)
