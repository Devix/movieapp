package com.android.movieapp.presentation.map

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.android.movieapp.R
import com.android.movieapp.databinding.FragmentMapBinding
import com.android.movieapp.presentation.base.BaseFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MapFragment : BaseFragment() {
    var mMap: GoogleMap? = null

    private val callback = OnMapReadyCallback { googleMap ->
        mMap = googleMap
    }
    private val mapViewModel: MapViewModel by viewModels()
    private lateinit var viewDataBinding: FragmentMapBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewDataBinding =
            FragmentMapBinding.inflate(inflater, container, false)
                .apply {
                    viewmodel = mapViewModel
                    lifecycleOwner = this@MapFragment.viewLifecycleOwner
                }

        return viewDataBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    override fun setupViewModelObservers() {
        mapViewModel.locationLiveData.observe(viewLifecycleOwner) {
            it?.let {
                it.forEach {
                    val latLng = LatLng(it.latitude.toDouble(), it.longitude.toDouble())
                    mMap?.addMarker(MarkerOptions().position(latLng).title(it.dateLocation))
                        ?.showInfoWindow()
                    mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 20F));

                }

            }
        }
    }


}
