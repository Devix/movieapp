package com.android.movieapp.data.source.remote

import android.location.Location
import android.net.Uri
import com.android.movieapp.domain.model.LocationLocal
import com.android.movieapp.util.Constants
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Carlos Anguiano on 27/02/2022.
 * c.joseanguiano@gmail.com
 */
class FirebaseDataSource(private val firebaseFirestore: FirebaseFirestore) {

    private val date = SimpleDateFormat("ddMM", Locale.getDefault()).format(Date()).toString()
    private val simpleDateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())

    fun uploadLocation(location: Location) {
        firebaseFirestore.collection(date)
            .document(System.currentTimeMillis().toString()).set(
                hashMapOf(
                    Constants.DATE_LOCATION to simpleDateFormat.format(location.time).toString(),
                    Constants.LATITUDE to location.latitude.toString(),
                    Constants.LONGITUDE to location.longitude.toString()
                )
            ).addOnSuccessListener {
            }
    }

    fun getLastLocation(
        success: (locationLocal: ArrayList<LocationLocal>?) -> Unit
    ) {
        val listLocation: ArrayList<LocationLocal> = ArrayList()
        firebaseFirestore.collection(date).addSnapshotListener { snapshot, _ ->
            if (snapshot != null) {
                val documents = snapshot.documents
                documents.forEach {
                    val locationLocal = it.toObject(LocationLocal::class.java)
                    if (locationLocal != null) {
                        listLocation.add(locationLocal)
                    }
                }
                success(listLocation)
            }
        }
    }

    fun uploadImageToFirebase(fileUri: Uri, success: (data: Boolean) -> Unit) {
        val fileName = UUID.randomUUID().toString() + ".jpg"

        val refStorage = FirebaseStorage.getInstance().reference.child("images/$fileName")

        refStorage.putFile(fileUri)
            .addOnSuccessListener { taskSnapshot ->
                taskSnapshot.storage.downloadUrl.addOnSuccessListener {
                    it.toString()
                    success(true)
                }
            }
            .addOnFailureListener {
                success(false)
            }

    }

}