package com.android.movieapp.data.source.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.android.movieapp.data.source.local.dao.MovieDao
import com.android.movieapp.data.source.local.entity.MovieEntity


/**
 * Created by Carlos Anguiano on 25/02/2022.
 * c.joseanguiano@gmail.com
 */
@Database(entities = [MovieEntity::class], version = 1, exportSchema = false)
abstract class MovieDataBase : RoomDatabase() {
    abstract val movieDao: MovieDao
}