package com.android.movieapp.data.repository

import android.net.Uri
import com.android.movieapp.data.source.remote.FirebaseDataSource
import com.android.movieapp.domain.repository.GalleryRepository


/**
 * Created by Carlos Anguiano on 27/02/2022.
 * c.joseanguiano@gmail.com
 */
class GalleryRepositoryImp(
    private val firebaseDataSource: FirebaseDataSource
) : GalleryRepository {

    override suspend fun uploadImageToFirebase(
        currentPhotoPath: Uri,
        successs: (data: Boolean?) -> Unit
    ) {
        firebaseDataSource.uploadImageToFirebase(currentPhotoPath) { success ->
            successs(success)
        }
    }
}
