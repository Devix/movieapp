package com.android.movieapp.data.source.remote

import com.android.movieapp.domain.model.Album
import com.android.movieapp.util.Constants.API_VERSION
import io.reactivex.Single
import retrofit2.http.GET


/**
 * Created by Carlos Anguiano on 25/02/2022.
 * c.joseanguiano@gmail.com
 */
interface RetrofitService {

    @GET("/$API_VERSION/movie/popular")
    fun getAlbums(): Single<Album>

}