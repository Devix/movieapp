package com.android.movieapp.data.source.local.dao

import androidx.room.*
import com.android.movieapp.data.source.local.entity.MovieEntity


/**
 * Created by Carlos Anguiano on 25/02/2022.
 * c.joseanguiano@gmail.com
 */
@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(movieEntity: MovieEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(movieEntity: List<MovieEntity>)

    @Query("SELECT * FROM Movie")
    fun loadAllMovie(): List<MovieEntity>

    @Delete
    fun deleteMovie(movieEntity: MovieEntity)

    @Query("DELETE FROM Movie")
    fun deleteAllMovie()

    @Query("SELECT * FROM Movie WHERE id = :movieId")
    fun getMovieByMovieId(movieId: Long): MovieEntity?

    @Update
    fun update(movieEntity: MovieEntity)
}