package com.android.movieapp.data.mapper

import com.android.movieapp.data.source.local.entity.MovieEntity
import com.android.movieapp.domain.model.Movie


/**
 * Created by Carlos Anguiano on 25/02/2022.
 * c.joseanguiano@gmail.com
 */
fun Movie.toEntity() = MovieEntity(
    id = id,
    title = title,
    url = posterPath,
    thumbnailUrl = posterPath
)


fun List<Movie>.toEntityList(): List<MovieEntity> {
    val listMovieEntity: MutableList<MovieEntity> = arrayListOf()

    for (i in indices) {
        listMovieEntity.add(
            MovieEntity(
                id = get(i).id,
                title = get(i).title,
                url = get(i).posterPath,
                thumbnailUrl = get(i).posterPath
            )
        )
    }
    return listMovieEntity
}


fun List<MovieEntity>.toMovieList(): List<Movie> {
    val listMovie: MutableList<Movie> = arrayListOf()

    for (i in indices) {
        listMovie.add(
            Movie(
                id = get(i).id,
                title = get(i).title,
                posterPath = get(i).thumbnailUrl
            )
        )
    }
    return listMovie
}