package com.android.movieapp.data.source.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey


/**
 * Created by Carlos Anguiano on 25/02/2022.
 * c.joseanguiano@gmail.com
 */
@Entity(tableName = "Movie")
data class MovieEntity(
    @PrimaryKey var id: Int,
    var title: String,
    val url: String,
    val thumbnailUrl: String
)