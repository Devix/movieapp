package com.android.movieapp.data.repository

import com.android.movieapp.data.source.remote.FirebaseDataSource
import com.android.movieapp.domain.model.LocationLocal
import com.android.movieapp.domain.repository.LocationRepository


/**
 * Created by Carlos Anguiano on 25/02/2022.
 * c.joseanguiano@gmail.com
 */
class LocationRepositoryImp(
    private val firebaseDataSource: FirebaseDataSource
) : LocationRepository {

    override suspend fun getPotatoesData(success: (listLocation: ArrayList<LocationLocal>?) -> Unit) {
        firebaseDataSource.getLastLocation { potatoesData ->
            if (potatoesData != null) {
                success(potatoesData)
            }
        }
    }
}
