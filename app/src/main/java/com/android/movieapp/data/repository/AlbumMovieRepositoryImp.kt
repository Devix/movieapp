package com.android.movieapp.data.repository

import android.location.Location
import com.android.movieapp.data.mapper.toEntityList
import com.android.movieapp.data.mapper.toMovieList
import com.android.movieapp.data.source.local.db.MovieDataBase
import com.android.movieapp.data.source.remote.FirebaseDataSource
import com.android.movieapp.data.source.remote.RetrofitService
import com.android.movieapp.domain.model.Album
import com.android.movieapp.domain.model.Movie
import com.android.movieapp.domain.repository.AlbumRepository
import io.reactivex.Single


/**
 * Created by Carlos Anguiano on 25/02/2022.
 * c.joseanguiano@gmail.com
 */
class AlbumMovieRepositoryImp(
    private val database: MovieDataBase,
    private val retrofitService: RetrofitService,
    private val firebaseDataSource: FirebaseDataSource
) : AlbumRepository {


    override fun getAlbumsMovie(): Single<Album> {
        return retrofitService.getAlbums()
    }

    override fun saveAllMovies(results: List<Movie>) {
        database.movieDao.insertAll(results.toEntityList())
    }

    override fun getAllAlbumLocal(): List<Movie> {
        return database.movieDao.loadAllMovie().toMovieList()
    }

    override fun uploadLocationFirebase(location: Location) {
        return firebaseDataSource.uploadLocation(location)
    }
}

