package com.android.movieapp

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.multidex.MultiDex
import dagger.hilt.android.HiltAndroidApp


/**
 * Created by Carlos Anguiano on 25/02/2022.
 * c.joseanguiano@gmail.com
 */
@HiltAndroidApp
class MovieApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
    }
}