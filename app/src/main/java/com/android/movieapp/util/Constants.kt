package com.android.movieapp.util


/**
 * Created by Carlos Anguiano on 25/02/2022.
 * c.joseanguiano@gmail.com
 */
object Constants {
    const val DATA_BASE_NAME = "MovieDataBase.db"
    const val LATITUDE = "latitude"
    const val LONGITUDE = "longitude"
    const val DATE_LOCATION = "dateLocation"

    const val BASE_POST_MOVIE = "https://image.tmdb.org/t/p/w185"
    const val PARAM_API = "api_key"
    const val BASE_URL = "https://api.themoviedb.org/"
    const val API_VERSION: Int = 3

    const val PERMISSION_LOCATION: Int = 90
    const val PERMISSION_CAMERA: Int = 91
    const val CODE_RESULT_CAMERA: Int = 93
    const val CODE_RESULT_GALLERY: Int = 94

    const val NOTIFICATION_ID = 1
    const val ID_NOTIFICATION = "movie_id"
    const val NOTIFICATION = "notification movie"
}