package com.android.movieapp.di

import android.app.Application
import androidx.room.Room
import com.android.movieapp.data.source.local.dao.MovieDao
import com.android.movieapp.data.source.local.db.MovieDataBase
import com.android.movieapp.util.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


/**
 * Created by Carlos Anguiano on 25/02/2022.
 * c.joseanguiano@gmail.com
 */
@InstallIn(SingletonComponent::class)
@Module
class DataBaseModule {

    @Provides
    @Singleton
    internal fun provideDataBase(application: Application): MovieDataBase {
        return Room.databaseBuilder(
            application,
            MovieDataBase::class.java,
            Constants.DATA_BASE_NAME
        ).allowMainThreadQueries().build()
    }

    @Provides
    internal fun provideMovieDao(movieDataBase: MovieDataBase): MovieDao {
        return movieDataBase.movieDao
    }
}