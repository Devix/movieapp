package com.android.movieapp.di

import android.content.Context
import com.android.movieapp.BuildConfig
import com.android.movieapp.data.repository.AlbumMovieRepositoryImp
import com.android.movieapp.data.repository.GalleryRepositoryImp
import com.android.movieapp.data.repository.LocationRepositoryImp
import com.android.movieapp.data.source.local.db.MovieDataBase
import com.android.movieapp.data.source.remote.FirebaseDataSource
import com.android.movieapp.data.source.remote.RetrofitService
import com.android.movieapp.domain.repository.AlbumRepository
import com.android.movieapp.domain.repository.GalleryRepository
import com.android.movieapp.domain.repository.LocationRepository
import com.android.movieapp.presentation.util.NetworkConnectivity
import com.android.movieapp.util.Constants
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


/**
 * Created by Carlos Anguiano on 25/02/2022.
 * c.joseanguiano@gmail.com
 */
@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {

    @Provides
    @Singleton
    fun providesRetrofit(
        gsonConverterFactory: GsonConverterFactory,
        rxJava2CallAdapterFactory: RxJava2CallAdapterFactory,
        okHttpClient: OkHttpClient
    ): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(gsonConverterFactory)
            .addCallAdapterFactory(rxJava2CallAdapterFactory)
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun providesOkHttpClient(
        @ApplicationContext context: Context
    ): OkHttpClient {
        val cacheSize = (5 * 1024 * 1024).toLong()
        val mCache = Cache(context.cacheDir, cacheSize)
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder()
            .cache(mCache) // make your app offline-friendly without a database!
            .connectTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .addNetworkInterceptor(interceptor)
            .addInterceptor { chain ->
                val originalRequest = chain.request()
                val originalUrl = originalRequest.url
                val url = originalUrl.newBuilder()
                    .addQueryParameter(Constants.PARAM_API, BuildConfig.API_KEY)
                    .build()

                val requestBuilder = originalRequest.newBuilder().url(url)
                    .header("Cache-Control", "public, max-age=" + 5)
                val request = requestBuilder.build()
                chain.proceed(request)
            }
        return client.build()
    }

    @Provides
    @Singleton
    fun providesGson(): Gson {
        return Gson()
    }

    @Provides
    @Singleton
    fun providesGsonConverterFactory(): GsonConverterFactory {
        return GsonConverterFactory.create()
    }

    @Provides
    @Singleton
    fun providesRxJavaCallAdapterFactory(): RxJava2CallAdapterFactory {
        return RxJava2CallAdapterFactory.create()
    }

/*    @Provides
    @Singleton
    fun provideIsNetworkAvailable(@ApplicationContext context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnected
    }*/

    @Provides
    @Singleton
    fun providesNetworkConnectivity(@ApplicationContext context: Context): NetworkConnectivity {
        return NetworkConnectivity(context)
    }


    @Singleton
    @Provides
    fun provideService(retrofit: Retrofit): RetrofitService {
        return retrofit.create(RetrofitService::class.java)
    }

    @Singleton
    @Provides
    fun provideFirebaseInstance(): FirebaseFirestore {
        return FirebaseFirestore.getInstance()
    }

    @Singleton
    @Provides
    fun provideFirebaseDataSource(): FirebaseDataSource {
        return FirebaseDataSource(FirebaseFirestore.getInstance())
    }

    @Singleton
    @Provides
    fun provideAlbumRepository(
        appDatabase: MovieDataBase,
        retrofitService: RetrofitService,
        firebaseDataSource: FirebaseDataSource
    ): AlbumRepository {
        return AlbumMovieRepositoryImp(appDatabase, retrofitService, firebaseDataSource)
    }

    @Singleton
    @Provides
    fun provideLocationRepository(
        firebaseDataSource: FirebaseDataSource
    ): LocationRepository {
        return LocationRepositoryImp(firebaseDataSource)
    }

    @Singleton
    @Provides
    fun provideGalleryRepository(
        firebaseDataSource: FirebaseDataSource
    ): GalleryRepository {
        return GalleryRepositoryImp(firebaseDataSource)
    }
}
